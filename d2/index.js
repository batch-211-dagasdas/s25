console.log('helo world')

// json is a data format used by application to store and transpoty date to one another
	// not limited to JavaScript

// JSON OBJECT
/*
	-JSON = JavaScript Object Notation
	-not limited to JavaScript
	-JavaScript Object are not to be confused w/ JSON
	-Serrialization is the process of converting data into series of bytes for easier transmission/transfer of information
	-uses double qoutes for property Names

		Syntax:
			{
				"propertyA": "valueA",
				"propertyB": "valueB"
			}	

---Json Object
	"city": "Quezon City",
	"province": "Metro Manila",
	"Country": "philippines"


---Json Array


"cities":[
	{	
		"city": "Quezon city", "Provice": "metro manila","Country": "philippines"

	}
	{	
		"city": "manila city", "Provice": "metro manila","Country": "philippines"

	}
	{	
		"city": "Makati city", "Provice": "metro manila","Country": "philippines"

	}
]

*/

// -JSON Methods
//  The Json object contains methods for parsing and converting data into stringified Json
	
	// converting Data into Stringified JSON
		/*
			-stringified Json is a javaScript Object converting into a string to be used in other function of JS application
		*/

	let batchesArr = [{batchName: 'batchX'}, { batchName: 'batchY'}]
// the "Stringify" method is used to convert javasCript Object into a string

console.log('result from stringify method: ');
console.log(JSON.stringify(batchesArr));

let data =JSON.stringify({
	name: 'john',
	age: '31',
	address: {city: 'manila', country: 'philippines'}
})
console.log(data);

//  using Stringify method w/ variable
/*
	-when information is stored in a variable and is not hard coded into the object that is being stringified, we can supply the value w/ var
	-this is commonly used when th info to be stored and send to a backend application will come from frontend application
	-Syntax: 
	JSON.stringify(
			{
				"propertyA": "valueA",
				"propertyB": "valueB"
			}	
		)
*/

// user details

let firstName = prompt('what is your first name?');
let lastName = prompt('what is your last name?');
let age = prompt('what is your age?');
let address = {
	city: prompt('what city do you live?'),
	country: prompt('which country does your city address belong to ?')
}

let otherData = JSON.stringify({
	 firstName:firstName,
	 lastName:lastName,
	 age:age,
	 address: address

})
console.log(otherData)

// converting Stringified JSON into JAVASript object
/*
	-objects are common data types used in application bec of complex data structures that can be created out of them
	-information is commoly sent ot application into stringified JSON and then converted back into objects
	-this happens both for sending information to backend application and vice versa

*/

let batchesJSON = `[{"batchName": "batch X"},{"batchName": "batch Y"}] `;

console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{"name": "john", "age" : "31", "address":{"city": "manila" , "country": "philippines"} }`

console.log(JSON.parse(stringifiedObject));